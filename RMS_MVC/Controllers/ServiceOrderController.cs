﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMS_MVC.Models;

namespace RMS_MVC.Controllers
{
    [Authorize]
    public class ServiceOrderController : Controller
    {
        private RmsContext db = new RmsContext();

        //
        // GET: /ServiceOrder/

        public ActionResult Index()
        {
            var serviceorders = db.ServiceOrders.Include(s => s.Client);
            return View(serviceorders.ToList());
        }

        //
        // GET: /ServiceOrder/Details/5

        public ActionResult Details(int id = 0)
        {
            ServiceOrder serviceorder = db.ServiceOrders.Find(id);
            if (serviceorder == null)
            {
                return HttpNotFound();
            }
            return View(serviceorder);
        }

        //
        // GET: /ServiceOrder/Create

        public ActionResult Create()
        {
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "LastName");
            return View();
        }

        //
        // POST: /ServiceOrder/Create

        [HttpPost]
        public ActionResult Create(ServiceOrder serviceorder)
        {
            if (ModelState.IsValid)
            {
                db.ServiceOrders.Add(serviceorder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "LastName", serviceorder.ClientID);
            return View(serviceorder);
        }

        //
        // GET: /ServiceOrder/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ServiceOrder serviceorder = db.ServiceOrders.Find(id);
            if (serviceorder == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "LastName", serviceorder.ClientID);
            return View(serviceorder);
        }

        //
        // POST: /ServiceOrder/Edit/5

        [HttpPost]
        public ActionResult Edit(ServiceOrder serviceorder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceorder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "LastName", serviceorder.ClientID);
            return View(serviceorder);
        }

        //
        // GET: /ServiceOrder/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ServiceOrder serviceorder = db.ServiceOrders.Find(id);
            if (serviceorder == null)
            {
                return HttpNotFound();
            }
            return View(serviceorder);
        }

        //
        // POST: /ServiceOrder/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ServiceOrder serviceorder = db.ServiceOrders.Find(id);
            db.ServiceOrders.Remove(serviceorder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}