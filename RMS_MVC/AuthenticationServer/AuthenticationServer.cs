﻿using RMS_MVC.Models;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using WebMatrix.WebData;
using System.Web.Security;

namespace RMS_MVC
{
    public class AuthenticationServer
    {
        public static void Run()
        {
            TcpListener listner = null;

            try
            {
                Int32 port = 8081;
                IPAddress ipAddress = IPAddress.Parse("127.0.0.1");

                listner = new TcpListener(ipAddress, port);

                // Start listening for incoming client requets
                listner.Start();

                while (true)
                {
                    try
                    {
                        Socket socket = listner.AcceptSocket();

                        SocketServer ss = new SocketServer(socket);
                        Thread thread = new Thread(new ThreadStart(ss.Run));
                        thread.Start();

                        //socket.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("SocketException: {0}", e);
                    }
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Stop listening for new clients
                listner.Stop();
            }
        }
    }
}
