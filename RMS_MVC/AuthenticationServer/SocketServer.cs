﻿using RMS_MVC.Models;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Security;
using System.IO;

namespace RMS_MVC
{
    public class SocketServer
    {
        private Socket socket = null;

        public SocketServer(Socket socket)
        {
            this.socket = socket;
        }

        public void Run()
        {
            NetworkStream stream = new NetworkStream(socket);
            BinaryFormatter bf = new BinaryFormatter();
            bf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            object o = bf.Deserialize(stream);
            LoginModel login = o as LoginModel;

            bool result = Membership.ValidateUser(login.UserName, login.Password);

            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(result);

            socket.Close();
        }
    }
}
