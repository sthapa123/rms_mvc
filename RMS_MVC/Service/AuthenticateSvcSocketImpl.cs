﻿using RMS_MVC.Models;
using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace RMS_MVC.Service
{
    public class AuthenticateSvcSocketImpl : IAuthenticationSvc
    {
        public bool AuthenticateUser(LoginModel login)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8081);
                socket.Connect(ipEndPoint);
                NetworkStream stream = new NetworkStream(socket);

                BinaryFormatter bf = new BinaryFormatter();
                bf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
                bf.Serialize(stream, login);
                BinaryReader reader = new BinaryReader(stream);
                bool result = reader.ReadBoolean();
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }

            return false;
        }
    }
}
