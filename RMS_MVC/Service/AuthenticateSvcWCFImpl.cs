﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS_MVC.AuthServiceReference;

namespace RMS_MVC.Service
{
    public class AuthenticateSvcWCFImpl : IAuthenticationSvc
    {

        public bool AuthenticateUser(Models.LoginModel login)
        {
            AuthServiceReference.AuthenticationWcfSvcClient proxy = new AuthenticationWcfSvcClient();
            RMS_MVC.AuthServiceReference.Login wcfLogin = new RMS_MVC.AuthServiceReference.Login();
            wcfLogin.UserName = login.UserName;
            wcfLogin.Password = login.Password;
            wcfLogin.RememberMe = login.RememberMe;

            bool result = proxy.AuthenicateUser(wcfLogin);
            return result;
        }
    }
}