﻿using RMS_MVC.Models;


namespace RMS_MVC.Service
{
    public interface IAuthenticationSvc : IService
    {
        bool AuthenticateUser(LoginModel login);
    }
}
