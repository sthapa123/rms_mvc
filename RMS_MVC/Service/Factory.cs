﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace RMS_MVC.Service
{
    public class Factory
    {
        #region Fields
        /// <summary>
        /// A static reference to the factory
        /// </summary>
        private static Factory factory = new Factory();
        #endregion

        #region Constructor
        /// <summary>
        /// A hidden constructor for the class.
        /// This prevents it being created outside of the class
        /// </summary>
        private Factory()
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns an instance of the factory
        /// </summary>
        /// <returns>The factory</returns>
        public static Factory GetInstance()
        {
            return factory;
        }

        /// <summary>
        /// Returns the requested service
        /// </summary>
        /// <param name="serviceName">The service that is being requested</param>
        /// <returns>The requested service as an IService object, must to cast to correct type</returns>
        public IService GetService(string serviceName)
        {
            Type type;
            Object obj = null;

            try
            {
                type = Type.GetType(GetImplName(serviceName));
                obj = Activator.CreateInstance(type);
            }
            catch (Exception e)
            {
                Console.WriteLine("Excetpion occured: {0}", e);
                throw e;
            }

            return (IService)obj;
        }

        /// <summary>
        /// Returns the implementation name of the requested service
        /// </summary>
        /// <param name="serviceName">The service that is needed</param>
        /// <returns>The name of the implementation of the service requested</returns>
        public string GetImplName(string serviceName)
        {
            NameValueCollection settings = ConfigurationManager.AppSettings;

            return settings.Get(serviceName);
        }

        #endregion
    }
}
