﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_MVC.Service
{
    public class AuthenticateSvcWSImpl : IAuthenticationSvc
    {

        public bool AuthenticateUser(Models.LoginModel login)
        {
            RMS_MVC.AuthenticationSvc.AuthenticationSvc authSvc = new RMS_MVC.AuthenticationSvc.AuthenticationSvc();

            bool result = authSvc.AuthenicateUser(login.UserName, login.Password);

            return result;
        }
    }
}