﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS_MVC.Service;
using RMS_MVC.Models;

namespace RMS_MVC.Business
{
    public class AuthenticationManager : Manager
    {
        private IAuthenticationSvc authenticationSvc;

        public AuthenticationManager()
        {
            authenticationSvc = (IAuthenticationSvc)getService("IAuthenticationSvc");
        }

        public bool AuthenticateUser(LoginModel login)
        {
            return authenticationSvc.AuthenticateUser(login);
        }
    }
}