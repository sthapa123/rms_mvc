﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS_MVC.Service;

namespace RMS_MVC.Business
{
    public abstract class Manager
    {
        private Factory factory = Factory.GetInstance();

        protected IService getService(string name)
        {
            return factory.GetService(name);
        }
    }
}