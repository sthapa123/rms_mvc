﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_MVC.Models
{
    public class ServiceOrder
    {
        public int ServiceOrderID { set; get; }
        public int ClientID { set; get; }
        public string Manufacturer { set; get; }
        public string Model { set; get; }
        public string SerialNumber { set; get; }
        public string ProblemDescription { set; get; }
        public string RepairComments { set; get; }
        public virtual Client Client { get; set; }
    }
}