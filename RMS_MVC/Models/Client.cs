﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_MVC.Models
{
    public class Client
    {
        public int ClientID { set; get; }
        public string LastName { set; get; }
        public string FirstName { set; get; }
        public string Address { set; get; }
        public string City { set; get; }
        public string State { set; get; }
        public int ZipCode { set; get; }
        public string PhoneNumber { set; get; }
        public string EmailAddress { set; get; }
        public virtual ICollection<ServiceOrder> ServiceOrders { set; get; }
    }
}