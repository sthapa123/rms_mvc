﻿using System.Data.Entity;

namespace RMS_MVC.Models
{
    public class RmsContext : DbContext
    {
        public DbSet<Client> Clients { set; get; }
        public DbSet<ServiceOrder> ServiceOrders { set; get; }
        public DbSet<UserProfile> UserProfiles { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<RmsContext, Configuration>());
        }
    }
}