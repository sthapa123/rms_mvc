﻿using System.Data.Entity.Migrations;

namespace RMS_MVC.Models
{
    public class Configuration : DbMigrationsConfiguration<RmsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
    }
}