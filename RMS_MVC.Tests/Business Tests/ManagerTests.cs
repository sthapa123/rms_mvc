﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RMS_MVC.Service;

namespace RMS_MVC.Tests.Business_Tests
{
    [TestClass]
    public class ManagerTests
    {
        [TestMethod]
        public void GetService()
        {
            IService service = (IService)Factory.GetInstance().GetService("IAuthenticationSvc");

            Assert.AreEqual(service.GetType().Name, "AuthenticateSvcDefaultImpl");
        }
    }
}
