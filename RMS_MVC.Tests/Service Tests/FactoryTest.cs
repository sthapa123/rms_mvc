﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RMS_MVC.Service;

namespace RMS_MVC.Tests.Service_Tests
{
    [TestClass]
    public class FactoryTest
    {
        [TestMethod]
        public void GetServiceTest()
        {
            IService service = (IService)Factory.GetInstance().GetService("IAuthenticationSvc");

            Assert.AreEqual(service.GetType().Name, "AuthenticateSvcDefaultImpl");
        }

        [TestMethod]
        public void GetImplTest()
        {
            string retrievedImpl = Factory.GetInstance().GetImplName("IAuthenticationSvc");

            Assert.AreEqual(retrievedImpl, "RMS_MVC.Service.AuthenticateSvcDefaultImpl");
        }
    }
}
